# Nofaro Teste - API

## Project setup

1. Clone project
```
git clone https://gitlab.com/thiago0112/teste-nofaro-api
```
2. Change directory to docker
```
cd teste-nofaro-api/docker
```
3. Run docker
```
docker-compose up -d
```
4. Enter docker container
```
docker exec -it nofarotest-php bash
```
5. Install dependencies
```
composer install
```
6. Copy env file
```
cp .env.example .env
```
7. Generaate app key
```
php artisan key:generate
```
8. Run migrations
```
php artisan migrate
```