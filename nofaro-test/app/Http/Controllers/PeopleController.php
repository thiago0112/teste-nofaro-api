<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SavePersonRequest;
use App\Services\PeopleService;

class PeopleController extends Controller
{
    /**
     * People service
     *
     * @var App\Services\PeopleService
     */
    private $peopleService;

    public function __construct(PeopleService $peopleService) {
        $this->peopleService = $peopleService;
    }

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'people' => $this->peopleService->findMany(collect($request->all()))
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SavePersonRequest $request)
    {
        $newPerson = $this->peopleService->create($request->all());

        return response()->json([
            'id' => $newPerson->id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SavePersonRequest $request, $id)
    {
        $this->peopleService->update($id, $request->all());

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->peopleService->delete($id);

        return response()->json();
    }
}
