<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;

class Cors
{
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->isMethod(Request::METHOD_OPTIONS)) {
            return $this->addHeaders(new JsonResponse('', 204), true);
        }

        return $this->addHeaders($next($request));
    }

    /**
     * Add response cors headers
     *
     * @param Response $response
     * @return void
     */
    protected function addHeaders(Response $response, $preflight = false): Response
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
        ];

        if ($preflight) {
            $headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization';
            $headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS';
        }

        $response->headers->add($headers);

        return $response;
    }
}
