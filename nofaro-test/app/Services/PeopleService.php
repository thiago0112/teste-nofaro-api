<?php

namespace App\Services;

use App\Models\Person;
use Illuminate\Support\Collection;

class PeopleService
{
    /**
     * Eloquent model
     *
     * @var App\Models\Person
     */
    protected $model;

    public function __construct(Person $person)
    {
        $this->model = $person;
    }

    /**
     * Find many by custom filters
     *
     * @param Collection $filters
     * @return Collection
     */
    public function findMany(Collection $filters): Collection
    {
        $query = $this->model->orderBy('name');

        if ($filters->has('name')) {
            $query->where('name', 'like', "%{$filters->get('name')}%");
        }
        if ($filters->has('email')) {
            $query->orWhere('email', $filters->get('email'));
        }

        return $query->get();
    }

    /**
     * Create new person
     *
     * @param array $data
     * @return App\Models\Person
     */
    public function create(array $data): Person
    {
        $newPerson = new Person();
        $newPerson->fill($data);
        $newPerson->save();

        return $newPerson;
    }

    /**
     * Update person
     *
     * @param integer $id Person identifier
     * @param Collection $data Data to update
     * @return App\Models\Person
     */
    public function update($id, array $data): Person
    {
        $person = $this->model->findOrFail($id);

        $person->fill($data);

        $person->save();

        return $person;
    }

    /**
     * Delete person
     *
     * @param integer $id
     * @return void
     */
    public function delete($id): void
    {
        $person = $this->model->findOrFail($id);
        $person->delete();
    }
}
