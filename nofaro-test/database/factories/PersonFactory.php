<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    $faker->locale('pt_BR');
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'area_code' => $faker->areaCode,
        'phone_number' => $faker->cellphone(false, true)
    ];
});
