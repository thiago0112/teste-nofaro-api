<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Person;

class DeletePersonTest extends TestCase
{
    use RefreshDatabase;

    public function testDeletePerson()
    {
        $person = factory(Person::class)->create();

        $response = $this->json(
            'DELETE',
            "/api/v1/people/{$person->id}"
        );

        $this->assertDatabaseMissing('people', $person->toArray());
        $response->assertStatus(200);
    }

    public function testDeleteUnknowPersonShouldFail()
    {
        $response = $this->json(
            'DELETE',
            "/api/v1/people/22"
        );

        $response->assertStatus(404);
    }
}
