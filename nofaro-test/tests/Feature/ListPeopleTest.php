<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Person;

class ListPeopleTest extends TestCase
{
    use RefreshDatabase;

    public function testPeopleIndex()
    {
        $response = $this->get('/api/v1/people');

        $response->assertStatus(200);
    }

    public function testPeopleReturnsListSortedByName()
    {
        $people = factory(Person::class, 2)->create();
        $expectedResponse = [
            'people' => $people->sortBy('name')->values()->toArray()
        ];

        $response = $this->get('api/v1/people');

        $response
            ->assertStatus(200)
            ->assertJson($expectedResponse);
    }

    public function testSearchPersonByPartialName()
    {
        factory(Person::class, 2)->create();
        $expectedResponse = [
            'people' => factory(Person::class, 2)->create([
                'name' => 'John Doe'
            ])->toArray()
        ];

        $response = $this->json('GET',  '/api/v1/people', ['name' => 'Doe']);

        $response
            ->assertStatus(200)
            ->assertExactJson($expectedResponse);
    }

    public function testSearchPersonByExactEmail()
    {
        factory(Person::class, 2)->create();
        $expectedResponse = [
            'people' => factory(Person::class, 1)->create([
                'email' => 'johndoe@gmail.com'
            ])->toArray()
        ];

        $response = $this->json('GET',  '/api/v1/people', ['email' => 'johndoe@gmail.com']);

        $response
            ->assertStatus(200)
            ->assertExactJson($expectedResponse);
    }

    public function testSearchPersonByPartialEmailDoesntWork()
    {
        factory(Person::class, 1)->create([
            'email' => 'johndoe@gmail.com'
        ]);

        $expectedResponse = [
            'people' => []
        ];

        $response = $this->json('GET',  '/api/v1/people', ['email' => 'doe']);

        $response
            ->assertStatus(200)
            ->assertExactJson($expectedResponse);
    }

    public function testSearchPersonByNameOrEmail()
    {
        $expectedResponse = [
            'people' => [
                factory(Person::class)->create([
                    'name' => 'Jane Doe',
                    'email' => 'jane@gmail.com'
                ])->toArray(),
                factory(Person::class)->create([
                    'name' => 'John Doe',
                    'email' => 'john@gmail.com'
                ])->toArray()
            ]
        ];

        $response = $this->json('GET',  '/api/v1/people', [
            'name' => 'Doe',
            'email' => 'jane@gmail.com'
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson($expectedResponse);
    }
}

