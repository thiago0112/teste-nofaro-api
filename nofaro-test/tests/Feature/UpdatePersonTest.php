<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Person;

class UpdatePersonTest extends TestCase
{
    use RefreshDatabase;

    public function testOwnerCanUpdateHisEmail()
    {
        $person = factory(Person::class)->create()->toArray();

        $response = $this->json('PUT', '/api/v1/people/1', $person);

        $this->assertDatabaseHas('people', $person);

        $response->assertStatus(200);
    }

    public function testEmailShouldBeUnique()
    {
        factory(Person::class)->create(['email' => 'john@gmail.com']);

        $person = factory(Person::class)->create();

        $person->email = 'john@gmail.com';

        $response = $this->json(
            'PUT',
            "/api/v1/people/{$person->id}",
            $person->toArray()
        );

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('email');
    }

    public function testUpdatePerson()
    {
        $person = factory(Person::class)->create()->toArray();

        $person['name'] = 'John Doe';

        $response = $this->json('PUT', "/api/v1/people/{$person['id']}", $person);

        $this->assertDatabaseHas('people', $person);

        $response->assertStatus(200);
    }

    public function testUpdateUnknowPersonShouldFail()
    {
        $person = factory(Person::class)->make()->toArray();

        $response = $this->json('PUT', '/api/v1/people/1', $person);

        $response->assertStatus(404);
    }


    /**
     * @dataProvider inputValidation
     */
    public function testValidationRules($field, $value)
    {
        $person = factory(Person::class)->create();

        $person[$field] = $value;

        $response = $this->json(
            'PUT',
            "/api/v1/people/{$person->id}",
            $person->toArray()
        );

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors($field);
    }

    public function inputValidation()
    {
        return [
            'Name is required' => ['name', ''],
            'Name must have more than two characters' => ['name', 'ae'],
            'Email is required' => ['email', ''],
            'Email must have a valid format' => ['email', 'invalidemail'],
            'Area code must be a number' => ['area_code', 'aa'],
            'Area code must have more than one number' => ['area_code', 1],
            'Area code must have less than three numbers' => ['area_code', 123],
            'Phone number must be a number' => ['phone_number', 'aaaaaaaaaa'],
            'Phone numberm must have more then eight digits' => ['phone_number', 99999999],
            'Phone number must have less then ten digits' => ['phone_number', 9999999999],
        ];
    }
}
